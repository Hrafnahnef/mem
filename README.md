mem
===

mem - a colorized memory inspector for terminals and consoles

In essence a colorized alternative to the unix command _free_, but with htop-style output

Release history
--------------
* **v1.2: 2021-03-12 (current)** Bugfix release
* v1.1: 2021-03-12 Bugfix release
* v1.0: 2021-03-10
* v0-prerelease: 2014-02-07

Features
--------
* Full parameter compatibility with *free*
* Auto-detecting colorized terminal settings and shell piping. Possible to override with --color and --no-color
* Bars, graphs and percentages for displaying values, inspired by htop
* Easier to interpret
* "Fluid layout" that tries to fit into your terminal, even when terminal changes the size
* Cached and Buffer are considered as free memory (as in non-used)
* Choose between showing any combination of memory, swap, total (memory + swap combined) and Low/High memory (applicable for 32 bit systems)
* Specify your own color theme or default parameters using a `.memrc` file ($XDG_CONFIG_HOME/memrc, $HOME/.memrc or /etc/mem/memrc)
* Interactive mode

Screenshots
-----------

Full display with both mem and swap views, side-by-side:
![side-by-side graphs](https://bitbucket.org/Hrafnahnef/mem/raw/e059dda48734294f02b5d4f067f0e4ea61b78e05/img/1.png "Full display with both mem and swap")

Full width display on a system without swap space:
![smart skip for swaps](https://bitbucket.org/Hrafnahnef/mem/raw/e059dda48734294f02b5d4f067f0e4ea61b78e05/img/2.png "When swap is disregarded, memory takes full view")

"Responsive/fluid layout". `mem` will try desperately to fit in your terminal:
![responsive layout](https://bitbucket.org/Hrafnahnef/mem/raw/e059dda48734294f02b5d4f067f0e4ea61b78e05/img/3.png "The output will try to squeeze into whatever space it can find")

Live usage demo, including resizing terminal, brief output and --no-color:
![demo](https://bitbucket.org/Hrafnahnef/mem/raw/ab5d89a44f625bd7d7009f63a05231e1261ef7f1/img/screengrab-mem.gif "Live usage demo, including response/fluid layout")

"Color auto-detection". `mem` tries to auto-guess support for colorized output, but it can also be controlled manually:
![color detection](https://bitbucket.org/Hrafnahnef/mem/raw/8e88bfd4e873f83cf80624ead9ed9acef7b0b6b4/img/color-detection.png "Mem tries to auto-guess support for colorized output, but it can also be controlled manually")



Usage
-----

Basic usage

    mem

Enter interactive mode. Press *q* or *ESC* to quit.

    mem -i

Enter interactive mode for 10 seconds, with a refresh every 0.5 second, then quit automatically.

    mem -c 20 -d 5

Force color, useful when piping somewhere colors are supported or if auto-detect of color doesn't work:

    mem --color | cat
    mem -C | cat

Force disable color

    mem --color no
    mem --color=no
    mem -C no
    mem -n

In interactive mode, the following keys are supported:

     q or Q   Quit
     ESC      Quit
     c or C   Quit
     r        Force refresh
     b        Display values in bytes
     h or H   Display values in human-readable units, either traditional or SI
     k or K   Display values in KiB or Kb
     m or M   Display values in MiB or Mb
     g or G   Display values in GiB or Gb
     t or T   Display values in TiB or Tb
     p or P   Display values in PiB or Pb
     Y        Toggle showing memory
     S or s   Toggle showing swap
     j        Toggle showing total ("joined")
     l or L   Toggle showing Low and High memory
     w or W   Toggle showing wide output (each memory segment on its own line)
     v or V   Cycle verbosity up or down
     

Polling timeout can be specified in either seconds or deciseconds (tenths of a second). These are not commulative, but mutualy exclusive. The last one specified takes precedence.

    mem -d 5 # 5 deciseconds = 0.5 second
    mem -s 2 # 2 seconds
    mem -s 1 -d 5 # 0.5 second

Full list of supported options:

        -h      --help          Print help and quit.
        -u      --usage         Print usage and quit.
        -V      --version       Print version and quit.
        -L      --license       Print license and quit.
        -a      --author        Print authors and quit.
        -B      --brief         Be more brief.
        -v      --verbose       Be more verbose.
        -w      --wide          Use full width for every memory segment printed
        -x N    --max-width N   Limit output width of a memory segment to N characters 
        -n      --no-color      Don't use ANSI colored output.
        -C      --color=no|yes|auto
        -Y      --no-memory     Don't print RAM usage.
        -y      --mem           Print memory usage. This reverse --no-memory
        -S      --no-swap       Don't print swap usage.
                --swap          Print swap usage. This reverse --no-swap
        -j      --total         Print memory and swap combined into a total
        -l      --lohi          Print low and high memory (Only applicable for 32 bit systems)
        -c N    --count N       Only print N samples, then quit. 0 means never quit, same as -i. Default is 1 (same as free). Press q or ESC to quit.
        -i      --interactive   Enter interactive mode, print memory until keypress of q or ESC.
        -s N    --seconds N     Delay N seconds between every sample of memory. Defaults to 1.
        -d N    --delay N       Delay N deciseconds (tenths of a second) between every sample of memory. Defaults to 10 (1 second).
        -b      --bytes         Display values in bytes
        -K, -k  --kilo, --kibi  Display values in (SI) kb or (traditional) KiB
        -M, -m  --mega, --mebi  Display values in (SI) Mb or (traditional) MiB
        -G, -g  --giga, --gebi  Display values in (SI) Gb or (traditional) GiB
        -T, -t  --tera, --tebi  Display values in (SI) Tb or (traditional) TiB
        -P, -p  --peta, --pebi  Display values in (SI) Pb or (traditional) PiB
                --human         Display values using a traditional unit suited for humans
        -H      --si            Dusplay values using a SI unit suited for humans



Config
------
Create a file `memrc` in `$XDG_CONFIG_HOME` (or `$HOME/.memrc` or `/etc/mem/memrc`), with the following key=value format

    summary = 1;35;42
    normal = 32
    warning = 1;31
    alert = 1;37;41
    buffer = 1;33
    cached = 0;34
    flags = --all --default --command --line --parameters

values are specified as semicolon separated ANSI color codes.

Note, that the config file specification is scheduled to be changed significantly once mem version 3.0 is released

For your convenience, there is a template rc file in /usr/share/mem/memrc.template

Install
-------

    make
    make install

Source packages and builds
------
* 1.2 Arch Linux: [mem (AUR)](https://aur.archlinux.org/packages/mem/)
* 1.2 source ([gzip](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.2.tar.gz), [sig](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.0.tar.gz.sig)) 
* 1.2 source ([bz2](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.2.tar.bz2), [sig](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.0.tar.bz2.sig)) 
* 1.2 source ([xz](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.2.tar.xz), [sig](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.0.tar.xz.sig)) 
* 1.2 source ([zst](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.2.tar.zst), [sig](https://bitbucket.org/Hrafnahnef/mem/downloads/mem-1.0.tar.zst.sig)) 

Dependencies
------------
* glibc
* pthreads
* ncurses (for automatic checking for termcap color support)

Contribute
----------
* time
* code
* ideas
* rage
* ... it's all welcome!
 
Known bugs and improvements and roadmap:
----------------------------------------
* See the TODO file in the source

Changelog
=========
v1.2 2021-03-12
---------------
* Fix bug in wide output for lohi and total modes

v1.1 2021-03-12
---------------
* Add shorthand command line parameter for turning on memory (-y)
* Add missing parameters to help section
* Bugfix: Verbose mode doesn't shrink sufficiently for narrow terminals, causing overflows
* Bugfix: --max-width breaks --wide output for --total and --lohi memory segments
* Bugfix: --max-width doesn't work without --wide
* Bugfix: In oneshot mode, when selecting nothing to show (mem --no-mem --no-swap), mem waits for keyboard input before exiting
* Bugfix: --swap turns off memory (this should only be done with --only-swap)
* Bugfix: --mem turns off swap (this should only be done with --only-memory)

v1.0 2021-03-10
----
* First public stable release
* Fully feature compatible with free
