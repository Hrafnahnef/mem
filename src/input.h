#ifndef INPUT_H
#define INPUT_H

void* pollingLoop(void *arg);
void* keyboardEventLoop(void *arg);

#endif
