#include "MemoryRepresentation.h"

#include "strutils.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <sys/sysinfo.h>

using namespace std;

std::string MemoryRepresentation::getString(int str, int size, unsigned short int mode) const {
    switch (mode) {
        case MemoryRepresentation::MODE_SWAP:
            return this->_strings[str][size >= 5 ? 6 : size];
            break;
        case MemoryRepresentation::MODE_TOTAL:
            return this->_strings[str][size >= 5 ? 7 : size];
            break;
        case MemoryRepresentation::MODE_MEMORY: default:
            return this->_strings[str][size];
            break;
    }
}

std::string MemoryRepresentation::getLoHiText(int width, bool useColor, const ColorConfig &cc, CmdLineOpts *opts, int line) const {
    const unsigned long lowTotal = m_memory.total - m_memory.hightotal;
    const unsigned long lowFree  = m_memory.free  - m_memory.highfree;
    const unsigned long lowUsed  = lowTotal - lowFree;
    const unsigned long  hiTotal = m_memory.hightotal;
    const unsigned long  hiFree  = m_memory.highfree;
    const unsigned long  hiUsed  = hiTotal - hiFree;
    opts->fixateHumanReadableUnit(m_memory.total);
    
    const double lTotal  = 1024.0  * lowTotal / static_cast<double>(opts->m_unit);
    const double lUse    = 1024.0  * lowUsed  / static_cast<double>(opts->m_unit);
    const double lFree   = 1024.0  * lowFree  / static_cast<double>(opts->m_unit);
    const float  lTotalP =  100.0f * lowTotal / static_cast<double>(m_memory.total);
    const float  lUseP   =  100.0f * lowUsed  / static_cast<double>(m_memory.total);
    const float  lFreeP  =  100.0f * lowFree  / static_cast<double>(m_memory.total);
    const double hTotal  = 1024.0  *  hiTotal / static_cast<double>(opts->m_unit);
    const double hUse    = 1024.0  *  hiUsed  / static_cast<double>(opts->m_unit);
    const double hFree   = 1024.0  *  hiFree  / static_cast<double>(opts->m_unit);
    const float  hTotalP =  100.0f *  hiTotal / static_cast<double>(m_memory.total);
    const float  hUseP   =  100.0f *  hiUsed  / static_cast<double>(m_memory.total);
    const float  hFreeP  =  100.0f *  hiFree  / static_cast<double>(m_memory.total);

    std::ostringstream formatter;
    std::string val[2][2][3];
    formatter << setprecision(1) << fixed << lTotal;
    val[0][0][0] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << lTotalP << "%";
    val[0][1][0] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << lUse;
    val[0][0][1] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << lUseP << "%";
    val[0][1][1] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << lFree;
    val[0][0][2] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << lFreeP << "%";
    val[0][1][2] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << hTotal;
    val[1][0][0] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << hTotalP << "%";
    val[1][1][0] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << hUse;
    val[1][0][1] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << hUseP << "%";
    val[1][1][1] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << hFree;
    val[1][0][2] = formatter.str();
    formatter.str("");
    formatter.clear();
    formatter << setprecision(1) << fixed << hFreeP << "%";
    val[1][1][2] = formatter.str();
    formatter.str("");
    formatter.clear();

    int size = 6;
    int len = 0;
    int unit = 3;
    int padding = 2;
    std::string::size_type maxLabel = 0, maxValueLength = 0, maxPercLength = 0;
    bool shortest = false;
    int percentageCutoff = 2;
    std::ostringstream lineFormatter;
    std::ios_base::fmtflags reset(lineFormatter.flags());
    std::string noLoMemory = "No low memory.";
    std::string noHiMemory = "No high memory.";
    do {
        --size;
        if (size < 0) {
            size = 0;
            shortest = true;
        }
        if (size > 4) {
            unit = 4;
        } else if (size > 3) {
            padding = 1;
            unit = 3;
        } else if (size > 2) {
            padding = 1;
            unit = 2;
        } else if (size > 1) {
            padding = 1;
            unit = 1;
        } else  {
            padding = 1;
            unit = 0;
        }

        stringstream unitStringStream;
        std::string prefix = "M";
        std::string byte = "b";
        switch (opts->m_unit) {
            case 1L:    prefix = ""; break;
            case 1000L: prefix = "k"; break;
            case 1024L: prefix = "Ki"; byte="B"; break;
            case 1000000L: prefix = "M"; break;
            case 1024L * 1024L: prefix = "Mi"; byte="B"; break;
            case 1000000000L: prefix = "G"; break;
            case 1024L * 1024L * 1024L: prefix = "Gi"; byte="B"; break;
            case 1000000000000L: prefix = "T"; break;
            case 1024L * 1024L * 1024L * 1024L: prefix = "Ti"; byte="B"; break;
            case 1000000000000000L: prefix = "P"; break;
            case 1024L * 1024L * 1024L * 1024L * 1024L: prefix = "Pe"; byte="B"; break;
        }
        int prefixLength = prefix.length();

        switch (unit) {
            case 4:  unitStringStream << " " << prefix << byte << (prefixLength < 2 ? " " : "") << (prefixLength < 1 ? " " : ""); break;
            case 3:  unitStringStream << (prefixLength < 2 ? " " : "") << prefix << byte << (prefixLength < 1 ? " " : ""); break;
            case 2:  unitStringStream << (prefixLength < 1 ? " " : "") << prefix << (prefixLength < 2 ? byte : ""); break;
            case 1:  unitStringStream << (prefixLength > 1 ? prefix.substr(0, 1) : prefix) << (prefixLength < 1 ? byte : "");   break;
            default: break;
        }

        std::string loLabels[3] = {
            getString(STR_LO_TOTAL, size, MODE_LOHI),
            getString(STR_LO_USE,   size, MODE_LOHI),
            getString(STR_LO_FREE,  size, MODE_LOHI)
        };
        std::string hiLabels[3] = {
            getString(STR_HI_TOTAL, size, MODE_LOHI),
            getString(STR_HI_USE,   size, MODE_LOHI),
            getString(STR_HI_FREE,  size, MODE_LOHI)
        };
        // @TODO: mb-safe length
        std::string::size_type maxLoLabel = (*std::max_element(loLabels, loLabels + 3,   [](const std::string& l, const std::string& r) { return l.size() < r.size(); })).size();
        std::string::size_type maxHiLabel = (*std::max_element(hiLabels, hiLabels + 3,   [](const std::string& l, const std::string& r) { return l.size() < r.size(); })).size();
        std::string::size_type maxLoVal   = (*std::max_element(val[0][0], val[0][0] + 3, [](const std::string& l, const std::string& r) { return l.size() < r.size(); })).size();
        std::string::size_type maxHiVal   = (*std::max_element(val[1][0], val[1][0] + 3, [](const std::string& l, const std::string& r) { return l.size() < r.size(); })).size();
        std::string::size_type maxLoPerc  = (*std::max_element(val[0][1], val[0][1] + 3, [](const std::string& l, const std::string& r) { return l.size() < r.size(); })).size();
        std::string::size_type maxHiPerc  = (*std::max_element(val[1][1], val[1][1] + 3, [](const std::string& l, const std::string& r) { return l.size() < r.size(); })).size();

        lineFormatter.str("");
        lineFormatter.clear();
        if (0 == line && VERBOSITY_NORMAL == opts->m_verbosity) {
            std::string::size_type unitLength = static_cast<std::string::size_type>(unitStringStream.str().size());
            const std::string::size_type loWidth = 0 == lowTotal ? noLoMemory.size() : loLabels[0].size() + val[0][0][0].size() + unitLength + loLabels[1].size() + val[0][0][1].size() + unitLength + loLabels[2].size() + val[0][0][2].size() + unitLength + 2 * padding;
            const std::string::size_type hiWidth = 0 ==  hiTotal ? noHiMemory.size() : hiLabels[0].size() + val[1][0][0].size() + unitLength + hiLabels[1].size() + val[1][0][1].size() + unitLength + hiLabels[2].size() + val[1][0][2].size() + unitLength + 2 * padding;
            int leftover = width - static_cast<int>(loWidth) - static_cast<int>(hiWidth);
            if (leftover < 0) {
                continue;
            }

            for (int col = 0; col < 2; ++col) {
                if (0 == (col ? hiTotal : lowTotal)) {
                    lineFormatter << (col ? noHiMemory : noLoMemory);
                } else {
                    lineFormatter << (col ? hiLabels : loLabels)[0];
                    lineFormatter << (useColor ? cc.getAnsiCode(col ? ColorConfig::COLOR_HI_TOTAL : ColorConfig::COLOR_LO_TOTAL) : "");
                    lineFormatter << val[col][0][0];
                    lineFormatter << (useColor ? "\e[0m" : "");
                    lineFormatter << unitStringStream.str();
                    lineFormatter << (padding > 0 ? std::string(padding, ' ') : "");
                    lineFormatter << (col ? hiLabels : loLabels)[1];
                    lineFormatter << (useColor ? cc.getAnsiCode(col ? ColorConfig::COLOR_HI_USED : ColorConfig::COLOR_LO_USED) : "");
                    lineFormatter << val[col][0][1];
                    lineFormatter << (useColor ? "\e[0m" : "");
                    lineFormatter << unitStringStream.str();
                    lineFormatter << (padding > 0 ? std::string(padding, ' ') : "");
                    lineFormatter << (col ? hiLabels : loLabels)[2];
                    lineFormatter << (useColor ? cc.getAnsiCode(col ? ColorConfig::COLOR_HI_FREE : ColorConfig::COLOR_LO_FREE) : "");
                    lineFormatter << val[col][0][2];
                    lineFormatter << (useColor ? "\e[0m" : "");
                    lineFormatter << unitStringStream.str();
                }
                if (!col) {
                    lineFormatter << (leftover > 0 ? std::string(leftover, ' ') : "");
                }
            }
            return lineFormatter.str();
        } else {
            switch (line) {
                case 0: case 1: case 2: {
                    const std::string::size_type loWidth = 0 == lowTotal ? noLoMemory.size() : maxLoLabel + maxLoVal + static_cast<std::string::size_type>(unitStringStream.str().size()) + static_cast<std::string::size_type>(1) + maxLoPerc;
                    const std::string::size_type hiWidth = 0 ==  hiTotal ? noHiMemory.size() : maxHiLabel + maxHiVal + static_cast<std::string::size_type>(unitStringStream.str().size()) + static_cast<std::string::size_type>(1) + maxHiPerc;
                    int leftover = width - static_cast<int>(loWidth) - static_cast<int>(hiWidth);
                    if (leftover < 0) {
                        if (shortest) {
                            break;
                        }
                        continue;
                    }
                    int loColor = ColorConfig::COLOR_LO_TOTAL;
                    int hiColor = ColorConfig::COLOR_HI_TOTAL;
                    switch (line) {
                        case 1:
                            loColor = ColorConfig::COLOR_LO_USED;
                            hiColor = ColorConfig::COLOR_HI_USED;
                            break;
                        case 2:
                            loColor = ColorConfig::COLOR_LO_FREE;
                            hiColor = ColorConfig::COLOR_HI_FREE;
                            break;
                    }
                    if (0 == lowTotal) {
                        lineFormatter << (0 == line ? noLoMemory : std::string(noLoMemory.size(), ' '));
                    } else {
                        lineFormatter << std::left  << std::setw(maxLoLabel) << loLabels[line];
                        lineFormatter << (useColor ? cc.getAnsiCode(loColor) : "");
                        lineFormatter << std::right << std::setw(maxLoVal)   << val[0][0][line];
                        lineFormatter << (useColor ? "\e[0m" : "");
                        lineFormatter << unitStringStream.str() << " ";
                        lineFormatter << (useColor ? cc.getAnsiCode(loColor) : "");
                        lineFormatter << std::right << std::setw(maxLoPerc)  << val[0][1][line];
                        lineFormatter << (useColor ? "\e[0m" : "");
                    }
                    lineFormatter << std::string(leftover, ' ');
                    if (0 == hiTotal) {
                        lineFormatter << (0 == line ? noHiMemory : std::string(noHiMemory.size(), ' '));
                    } else {
                        lineFormatter << std::left  << std::setw(maxHiLabel) << hiLabels[line];
                        lineFormatter << (useColor ? cc.getAnsiCode(hiColor) : "");
                        lineFormatter << std::right << std::setw(maxHiVal)   << val[1][0][line];
                        lineFormatter << (useColor ? "\e[0m" : "");
                        lineFormatter << unitStringStream.str() << " ";
                        lineFormatter << (useColor ? cc.getAnsiCode(hiColor) : "");
                        lineFormatter << std::right << std::setw(maxHiPerc)  << val[1][1][line];
                        lineFormatter << (useColor ? "\e[0m" : "");
                    }
                    return lineFormatter.str();
                    break;
                }
                case 3: case 4:
                    lineFormatter << std::string(width, ' ');
                    return lineFormatter.str();
                    break;
            }
        }
    } while (!shortest);
    return std::string(width, ' ');
}

MemoryRepresentation::MemoryRepresentation() {
    ifstream infile("/proc/meminfo");
    std::string line;
    m_memory.total = 0;
    m_memory.free = 0;
    m_memory.buffer = 0;
    m_memory.cached = 0;
    m_memory.swaptotal = 0;
    m_memory.swapfree = 0;
    m_memory.swapcached = 0;
    m_memory.hightotal = 0;
    m_memory.highfree = 0;
    while (getline(infile, line)) {
        std::string label;
        int val;
        istringstream stringstream(line);
        if (stringstream >> label >> val) {
            if (label == "MemTotal:") {
                m_memory.total += val;
            } else if (label == "MemFree:") {
                m_memory.free += val;
            } else if (label == "Buffers:") {
                m_memory.buffer += val;
                m_memory.free += val;
            } else if (label == "Cached:") {
                m_memory.cached += val;
                m_memory.free += val;
            } else if (label == "SwapTotal:") {
                m_memory.swaptotal += val;
            } else if (label == "SwapFree:") {
                m_memory.swapfree += val;
            } else if (label == "SwapCached:") {
                m_memory.swapcached += val;
                m_memory.swapfree += val;
            } else if (label == "HighTotal:") {
                m_memory.hightotal += val;
            } else if (label == "HighFree:") {
                m_memory.highfree += val;
            }
        }
    }
}

bool MemoryRepresentation::hasSwap() const {
    return m_memory.swaptotal > 0;
}

std::string MemoryRepresentation::getBar(int width, unsigned short int mode, bool useColor, ColorConfig &cc) const {
    unsigned long _total     = 0;
    unsigned long _free      = 0;
    unsigned long _cached    = 0;
    unsigned long _buffer    = 0;
    unsigned long _highTotal = 0;
    unsigned long _highFree  = 0;
    switch (mode) {
        case MODE_MEMORY: {
            _total     = m_memory.total;
            _highTotal = m_memory.hightotal;
            _free      = m_memory.free;
            _highFree  = m_memory.highfree;
            _cached    = m_memory.cached;
            _buffer    = m_memory.buffer;
            break;
        }
        case MODE_SWAP: {
            _total  = m_memory.swaptotal;
            _free   = m_memory.swapfree;
            _cached = m_memory.swapcached;
            _buffer = 0;
            break;
        }
        case MODE_TOTAL: {
            _total  = m_memory.total  + m_memory.swaptotal;
            _free   = m_memory.free   + m_memory.swapfree;
            _cached = m_memory.cached + m_memory.swapcached;
            _buffer = m_memory.buffer;
            break;
        }
        case MODE_LOHI:
            std::ostringstream r;
            const int lowFreeWidth = std::max(0, static_cast<int>(floor(width * (1.0 * (m_memory.free - m_memory.highfree) / (m_memory.total)) + 0.5)));
            const int  hiFreeWidth = std::max(0, static_cast<int>(floor(width * (1.0 * m_memory.highfree / m_memory.total) + 0.5)));
            const int  hiUsedWidth = std::max(0, static_cast<int>(floor(width * m_memory.hightotal / m_memory.total + 0.5) - hiFreeWidth));
            const int lowUsedWidth = std::max(0, width - lowFreeWidth - hiFreeWidth - hiUsedWidth);
            r << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_LO_USED)    : "") << repeat(lowUsedWidth, "|")
                << (useColor ? "\e[0m"    : "")                                   << std::string(lowFreeWidth, ' ')
                << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_HI_USED) : "") << repeat( hiUsedWidth, "¦")
                << (useColor ? "\e[0m"    : "")                                   << std::string( hiFreeWidth, ' ');
            return r.str();
            break;
    }
    const unsigned long total = _total, free = _free, cached = _cached, buffer = _buffer, highfree = _highFree, hightotal = _highTotal;

    const int usedMarkers   = std::max(0, width - static_cast<int>(floor(width * (1.0 * free / total) + 0.5)));
    const int cacheMarkers  = std::max(0, static_cast<int>(floor(width * (1.0 * cached / total) + 0.5)));
    const int bufferMarkers = std::max(0, static_cast<int>(floor(width * (1.0 * buffer / total) + 0.5)));
    const int freeMarkers   = std::max(0, width - usedMarkers - cacheMarkers - bufferMarkers);

    /** syscall test */
    struct sysinfo test = {};
    sysinfo(&test);

    stringstream returnSS;
    returnSS << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_NORMAL) : "") << repeat(usedMarkers, "|")
                << (useColor ? "\e[0m"    : "") << std::string(freeMarkers, ' ')
                << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_BUFFER) : "") << std::string(bufferMarkers, '.')
                << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_CACHED) : "") << std::string(cacheMarkers, '.')
                << (useColor ? "\e[0m"    : "");
    return returnSS.str();
}

std::string MemoryRepresentation::getText(int width, unsigned short int mode, bool useColor, ColorConfig &cc, CmdLineOpts *opts, int line) const {
    int _total  = 0;
    int _free   = 0;
    int _cached = 0;
    int _buffer = 0;
    switch (mode) {
        case MODE_MEMORY: {
            _total  = m_memory.total;
            _free   = m_memory.free;
            _cached = m_memory.cached;
            _buffer = m_memory.buffer;
            break;
        }
        case MODE_SWAP: {
            _total  = m_memory.swaptotal;
            _free   = m_memory.swapfree;
            _cached = m_memory.swapcached;
            _buffer = 0;
            break;
        }
        case MODE_TOTAL: {
            _total  = m_memory.total  + m_memory.swaptotal;
            _free   = m_memory.free   + m_memory.swapfree;
            _cached = m_memory.cached + m_memory.swapcached;
            _buffer = m_memory.buffer;
            break;
        }
    }
    const int total = _total, free = _free, cached = _cached, buffer = _buffer;

    opts->fixateHumanReadableUnit(total);
    

    if (MODE_LOHI == mode) {
        return getLoHiText(width, useColor, cc, opts, line);
    }

    const double fTotal = (1024L * total) / static_cast<double>(opts->m_unit);
    const double fUse   = (1024L * total - 1024L * free) / static_cast<double>(opts->m_unit);
    const double fUseP  = round(10.0 * 100.0 * (1024L * total - 1024L * free) / static_cast<double>(1024L * total)) / 10.0;

    stringstream totVal, useVal, usePerc, freeVal, freePerc, buffVal, cachVal;
    totVal << setprecision(1) << fixed << fTotal;
    useVal << setprecision(1) << fixed << fUse;
    usePerc << setprecision(1) << fixed << fUseP;
    freeVal << setprecision(1) << fixed << (1024.0 * free / static_cast<double>(opts->m_unit));
    freePerc << setprecision(1) << fixed << (floor(10.0 * 100.0 * 1024L * free / (1024.0 * total))) / 10.0;
    buffVal << setprecision(1) << fixed << (1024.0 * buffer/static_cast<double>(opts->m_unit));
    cachVal << setprecision(1) << fixed << (1024.0 * cached/static_cast<double>(opts->m_unit));
    
    int size = 6;
    int len;
    int unit = 3;
    int padding = 2;
    std::string::size_type maxLabel = 0, maxValueLength = 0, maxPercLength = 0;
    bool shortest = false;
    int percentageCutoff = 2;
    do {
        --size;
        if (size < 0) {
            size = 0;
            shortest = true;
        }
        if (size > 4) {
            unit = 4;
        } else if (size > 3) {
            padding = 1;
            unit = 3;
        } else if (size > 2) {
            padding = 1;
            unit = 2;
        } else if (size > 1) {
            padding = 1;
            unit = 1;
        } else  {
            padding = 1;
            unit = 0;
        }
        len  = this->getString(STR_TOTAL, size, mode).length() + totVal.str().length() + unit + padding;
        len += this->getString(STR_USE,   size, mode).length() + useVal.str().length() + unit;
        if (size > percentageCutoff) {
            len += 1 + usePerc.str().length() + 1;
        }
        len += this->getString(STR_FREE,  size, mode).length() + freeVal.str().length() + unit + 1;
        if (size > percentageCutoff) {
            len += 1 + freePerc.str().length() + 1;
        }
        if (shortest) {
            break;
        }
        len += 2; // " ("
        if (buffer > 0 && MODE_MEMORY == mode) {
            len += this->getString(STR_BUFFER, size, mode).length() + buffVal.str().length() + unit;
            len += 2; // ", "
        }
        len += this->getString(STR_CACHE, size, mode).length() + cachVal.str().length() + unit;
        len += 1; // ")"
    } while (len >= width);
    
    if (VERBOSITY_VERBOSE == opts->m_verbosity) {
        size = 5;
        unit = 4;
        padding = 2;
        std::string::size_type labelLengths[] = {
            static_cast<std::string::size_type>(this->getString(STR_TOTAL,  size, mode).size()),
            static_cast<std::string::size_type>(this->getString(STR_USE,    size, mode).size()),
            static_cast<std::string::size_type>(this->getString(STR_FREE,   size, mode).size()),
            static_cast<std::string::size_type>(this->getString(STR_BUFFER, size, mode).size()),
            static_cast<std::string::size_type>(this->getString(STR_CACHE,  size, mode).size())
        };
        maxLabel = *std::max_element(labelLengths, labelLengths + 5);
        std::string::size_type valueLengths[] = {
            static_cast<std::string::size_type>(totVal.str().size()),
            static_cast<std::string::size_type>(useVal.str().size()),
            static_cast<std::string::size_type>(freeVal.str().size()),
            static_cast<std::string::size_type>(buffVal.str().size()),
            static_cast<std::string::size_type>(cachVal.str().size())
        };
        maxValueLength = *std::max_element(valueLengths, valueLengths + 5);
        std::string::size_type percLengths[] = {
            static_cast<std::string::size_type>(usePerc.str().size()),
            static_cast<std::string::size_type>(freePerc.str().size()),
        };
        maxPercLength = *std::max_element(percLengths, percLengths + 2);
        switch (line) {
            case 0: len = maxLabel + maxValueLength + unit + 1; break;
            case 1: len = maxLabel + maxValueLength + unit + 3 + maxPercLength; break;
            case 2: len = maxLabel + maxValueLength + unit + 3 + maxPercLength; break;
            case 3: len = maxLabel + maxValueLength + unit + 1; break;
            case 4: len = maxLabel + maxValueLength + unit + 1; break;
        }
    }

    stringstream totalSS, allocSS, freeSS, returnSS, paddingSS;
    std::string spaces = width - len + 1 > 0 ? std::string(width - len + 1, ' ') : "";
    for (; padding > 0; --padding) {
        paddingSS << ' ';
    }
    stringstream unitStringStream;
    std::string prefix = "M";
    std::string byte = "b";
    switch (opts->m_unit) {
        case 1L:    prefix = ""; break;
        case 1000L: prefix = "k"; break;
        case 1024L: prefix = "Ki"; byte="B"; break;
        case 1000000L: prefix = "M"; break;
        case 1024L * 1024L: prefix = "Mi"; byte="B"; break;
        case 1000000000L: prefix = "G"; break;
        case 1024L * 1024L * 1024L: prefix = "Gi"; byte="B"; break;
        case 1000000000000L: prefix = "T"; break;
        case 1024L * 1024L * 1024L * 1024L: prefix = "Ti"; byte="B"; break;
        case 1000000000000000L: prefix = "P"; break;
        case 1024L * 1024L * 1024L * 1024L * 1024L: prefix = "Pe"; byte="B"; break;
    }
    int prefixLength = prefix.length();
    
    switch (unit) {
        case 4:  unitStringStream << " " << prefix << byte << (prefixLength < 2 ? " " : "") << (prefixLength < 1 ? " " : ""); break;
        case 3:  unitStringStream << (prefixLength < 2 ? " " : "") << prefix << byte << (prefixLength < 1 ? " " : ""); break;
        case 2:  unitStringStream << (prefixLength < 1 ? " " : "") << prefix << (prefixLength < 2 ? byte : ""); break;
        case 1:  unitStringStream << (prefixLength > 1 ? prefix.substr(0, 1) : prefix) << (prefixLength < 1 ? byte : "");   break;
        default: break;
    }


    totalSS << this->getString(STR_TOTAL, size, mode) << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_SUMMARY) : "") << totVal.str() << (useColor ? "\e[0m"    : "") << unitStringStream.str();
    allocSS << this->getString(STR_USE, size, mode)
            << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_NORMAL) : "") << useVal.str() << (useColor ? "\e[0m" : "")
            << unitStringStream.str() << (size > percentageCutoff ? " " : "")
            << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_NORMAL) : "") << (size > percentageCutoff ? usePerc.str() : "")
            << (size > percentageCutoff ? "%" : "") << (useColor ? "\e[0m" : "");
    freeSS << this->getString(STR_FREE, size, mode)
            << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_SUMMARY) : "") << freeVal.str() << (useColor ? "\e[0m" : "")
            << unitStringStream.str() << (size > percentageCutoff ? " " : "")
            << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_SUMMARY) : "") << (size > percentageCutoff ? freePerc.str() : "")
            << (size > percentageCutoff ? "%" : "") << (useColor ? "\e[0m" : "");
    if (VERBOSITY_VERBOSE == opts->m_verbosity) {
        switch (line) {
            case 0: returnSS << this->getString(STR_TOTAL,  size, mode) << std::string(maxLabel - this->getString(STR_TOTAL,  size, mode).length() + maxValueLength -  totVal.str().size(), ' ') << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_SUMMARY) : "") <<  totVal.str() << (useColor ? "\e[0m" : "") << unitStringStream.str() << spaces; break;
            case 1: returnSS << this->getString(STR_USE,    size, mode) << std::string(maxLabel - this->getString(STR_USE,    size, mode).length() + maxValueLength -  useVal.str().size(), ' ') << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_NORMAL)  : "") <<  useVal.str() << (useColor ? "\e[0m" : "") << unitStringStream.str() << std::string(maxPercLength -  usePerc.str().size() + 1, ' ') << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_NORMAL)  : "") <<  usePerc.str() << "%" << (useColor ? "\e[0m" : "") << spaces; break;
            case 2: returnSS << this->getString(STR_FREE,   size, mode) << std::string(maxLabel - this->getString(STR_FREE,   size, mode).length() + maxValueLength - freeVal.str().size(), ' ') << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_SUMMARY) : "") << freeVal.str() << (useColor ? "\e[0m" : "") << unitStringStream.str() << std::string(maxPercLength - freePerc.str().size() + 1, ' ') << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_SUMMARY) : "") << freePerc.str() << "%" << (useColor ? "\e[0m" : "") << spaces; break;
            case 3: returnSS << this->getString(STR_BUFFER, size, mode) << std::string(maxLabel - this->getString(STR_BUFFER, size, mode).length() + maxValueLength - buffVal.str().size(), ' ') << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_BUFFER)  : "") << buffVal.str() << (useColor ? "\e[0m" : "") << unitStringStream.str() << spaces; break;
            case 4: returnSS << this->getString(STR_CACHE,  size, mode) << std::string(maxLabel - this->getString(STR_CACHE,  size, mode).length() + maxValueLength - cachVal.str().size(), ' ') << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_CACHED)  : "") << cachVal.str() << (useColor ? "\e[0m" : "") << unitStringStream.str() << spaces; break;
        }
        return returnSS.str();
    }
    if (!shortest) {
        freeSS << " (";
        if (buffer > 0 && MODE_MEMORY == mode) {
            freeSS << this->getString(STR_BUFFER, size, mode);
            freeSS << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_BUFFER) : "") << buffVal.str() << (useColor ? "\e[0m" : "") << unitStringStream.str();
            freeSS << ", ";
        }
        freeSS << this->getString(STR_CACHE, size, mode);
        freeSS << (useColor ? cc.getAnsiCode(ColorConfig::COLOR_CACHED) : "") << cachVal.str() << (useColor ? "\e[0m" : "") << unitStringStream.str();
        freeSS << ")";
    }
    returnSS << totalSS.str() << paddingSS.str() << allocSS.str() << spaces << freeSS.str();
    return returnSS.str();
}

MemoryRepresentation::~MemoryRepresentation() {}
