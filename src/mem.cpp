/******************************************************************************
 * Copyright (c) 2014-2021, Aron Cederholm
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CON-
 * SEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 ******************************************************************************/

#include <signal.h>
#include <pthread.h>

#include "CmdLineOpts.h"
#include "ColorConfig.h"
#include "input.h"
#include "MemoryRepresentation.h"
#include "PaintLoop.h"
#include "strutils.h"
#include "term.h"
#include "threadutils.h"

static PaintLoop* painter;

static void sigListener(int signal) {
    switch (signal) {
        case SIGWINCH:
			painter->resize();
            break;

        case SIGINT: case SIGHUP: case SIGTERM:
			painter->stop();
            break;
    }
}

void setupSigListeners() {
    if (signal((int) SIGINT, sigListener) == SIG_ERR) {
        throw std::runtime_error("Couldn't setup signal handlers");
    }
    if (signal((int) SIGHUP, sigListener) == SIG_ERR) {
        throw std::runtime_error("Couldn't setup signal handlers");
    }
    if (signal((int) SIGTERM, sigListener) == SIG_ERR) {
        throw std::runtime_error("Couldn't setup signal handlers");
    }
    if (signal((int) SIGWINCH, sigListener) == SIG_ERR) {
        throw std::runtime_error("Couldn't setup SIGWINCH signal handler");
    }
}

int main(int argc, const char** argv) {
	setupSigListeners();
	ColorConfig colorConfig;
	CmdLineOpts options(colorConfig, argc, argv);
	painter = new PaintLoop(&options, &colorConfig);

	pthread_mutex_lock(&lock);
	pthread_t keyboardlistener;
	pthread_t refreshThread;
	int err = pthread_create(&keyboardlistener, NULL, &keyboardEventLoop, painter);
	err = pthread_create(&refreshThread, NULL, &pollingLoop, painter);

	painter->start();

	// @TODO: Close threads
	pthread_mutex_unlock(&lock);
}
