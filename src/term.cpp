#include "term.h"

#include <cmath>
#include <iostream>
#include <sstream>
//#include <stdio.h>
#include <string>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

int getCols() {
	unsigned short int cols = 80;
	unsigned short int lines = 25;
#ifdef TIOCGSIZE
	struct ttysize ts;
	ioctl(STDIN_FILENO, TIOCGSIZE, &ts);
	cols = ts.ts_cols;
	lines = ts.ts_lines;
#elif defined(TIOCGWINSZ)
	struct winsize ws;
	if ((ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 &&
		ioctl(STDERR_FILENO, TIOCGWINSZ, &ws) == -1 &&
		ioctl(STDIN_FILENO,  TIOCGWINSZ, &ws) == -1) ||
		ws.ws_col == 0) {
	} else {
		lines = ws.ws_row;
		cols = ws.ws_col;
	}
#endif /* TIOCGSIZE */
	return cols;
}

int getLines() {
	unsigned short int cols = 80;
	unsigned short int lines = 25;
#ifdef TIOCGSIZE
	struct ttysize ts;
	ioctl(STDIN_FILENO, TIOCGSIZE, &ts);
	cols = ts.ts_cols;
	lines = ts.ts_lines;
#elif defined(TIOCGWINSZ)
	struct winsize ws;
	if ((ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 &&
		ioctl(STDERR_FILENO, TIOCGWINSZ, &ws) == -1 &&
		ioctl(STDIN_FILENO,  TIOCGWINSZ, &ws) == -1) ||
		ws.ws_col == 0) {
	} else {
		lines = ws.ws_row;
		cols = ws.ws_col;
	}
#endif /* TIOCGSIZE */
	return lines;
}

void printFrog() {
	int width = getCols();
	int leftspace = floor((width - 6) / 2);
	int rightspace = width - 6 - leftspace;
	std::stringstream left, right;
	for (int i = 0; i < leftspace; ++i) {
		left << ' ';
	}
	for (int i = 0; i < rightspace; ++i) {
		right << ' ';
	}
	int level = 0;
	int rows = getLines() - 2;
	int height = 4;
	int maxlegs = 2;
	int legs = 0;
	bool up = true;
	for (int i = 0; i < rows; ++i) {
		std::cout << std::endl;
	}
	while (level >= 0) {
		int rowsbelow = rows;
		// Goto first line
		for (int i = 0; i < rows; ++i) {
			std::cout << "\r\e[A";
		}
		// Paint lines of air above
		for (int lines = rows - level - legs - height; lines > 0; --lines, --rowsbelow) {
			std::cout << left.str() <<         "      "            << right.str() << "\r\n";
		}
		// Paint head
		rowsbelow -= 3;
		std::cout << left.str() << "\e[32m" << " @__@ " << "\e[0m" << right.str() << "\r\n";
		std::cout << left.str() << "\e[32m" << " (  ) " << "\e[0m" << right.str() << "\r\n";
		std::cout << left.str() << "\e[32m" << "(>__<)" << "\e[0m" << right.str() << "\r\n";
		// Paint legs
		for (int i = 0; i < legs; ++i, --rowsbelow) {
			std::cout << left.str() << "\e[32m" << "||  ||" << "\e[0m" << right.str() << "\r\n";
		}
		// Paint feet
		rowsbelow -= 1;
		std::cout << left.str() << "\e[32m" << "^^  ^^" << "\e[0m" << right.str() << "\r\n";
		// Paint lines of air below
		for (; rowsbelow > 0; --rowsbelow) {
			std::cout << left.str() <<         "      "            << right.str() << "\r\n";
		}
		// Calculate new frog position
		if (up) {
			if (level == 0 && legs < maxlegs) {
				++legs;
			} else if (level < rows - height) {
				++level;
				if (level + height + legs > rows) {
					--legs;
				}
			} else {
				up = false;
			}
		} else {
			--level;
		}
		// Sleep for some time
		int milliseconds = 15;
		struct timespec req = {0};
		req.tv_sec = 0;
		req.tv_nsec = static_cast<long>(milliseconds) * 1000000L;
		nanosleep(&req, (struct timespec *)NULL);
	}
}
