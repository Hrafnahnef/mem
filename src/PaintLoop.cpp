#include "PaintLoop.h"

#include "strutils.h"
#include "term.h"
#include "threadutils.h"

#include <cmath>
#include <cstring>
#include <iostream>
#include <exception>
#include <pthread.h>
#include <sstream>
#include <string>


std::string PaintLoop::getOutput(const MemoryRepresentation& sampleMemory, int terminalColumns, int dataColumns) const {
    std::ostringstream output, swapOutput, totalOutput, lohiOutput;

    // Clear frame
    int height = m_opts->height();
    std::string cls = std::string("\r\e[A");
    output << repeat(height, cls);

    bool memory = m_opts->m_show & SHOW_MEMORY;
    bool swap = (m_opts->m_show & SHOW_SWAP) && sampleMemory.hasSwap();
    std::string memBar   = memory          ? sampleMemory.getBar((m_maxOutputWidth) - 6, MemoryRepresentation::MODE_MEMORY, m_opts->m_color, *m_colorConfig) : "";
    std::string swapBar  = swap            ? sampleMemory.getBar((m_maxOutputWidth) - 7, MemoryRepresentation::MODE_SWAP,   m_opts->m_color, *m_colorConfig) : "";
    std::string totalBar = m_opts->m_total ? sampleMemory.getBar((m_maxOutputWidth) - 8, MemoryRepresentation::MODE_TOTAL,  m_opts->m_color, *m_colorConfig) : "";
    std::string lohiBar  = m_opts->m_lohi  ? sampleMemory.getBar((m_maxOutputWidth) - 7, MemoryRepresentation::MODE_LOHI,   m_opts->m_color, *m_colorConfig) : "";
    int spacers = dataColumns - 1;
    bool multispacers = spacers > 1;
    bool firstSpacer = true;
    unsigned int     columnSpacer = spacers ? (int)floor((m_terminalColumns - dataColumns * std::min(m_maxOutputWidth, m_width)) / spacers) : 0;
    unsigned int lastColumnSpacer = dataColumns > 2 ? m_terminalColumns - dataColumns * std::min(m_maxOutputWidth, m_width) - columnSpacer * (dataColumns - 2) : 0;
    std::string wideLineEndingsWhenLimited(m_opts->m_wide && m_maxOutputWidth < terminalColumns ? std::string(terminalColumns - m_maxOutputWidth, ' ') : "");
    if (memory) {
        output << std::string("Mem [") << memBar << "]" << wideLineEndingsWhenLimited.substr(0, std::max(0, terminalColumns - 6));
        firstSpacer = false;
    }
    if (memory && swap && !m_opts->m_wide) {
        --spacers;
        output << std::string(columnSpacer, ' ');
    }
    if (swap) {
        (m_opts->m_wide ? swapOutput : output) << "Swap [" << swapBar << "]" << wideLineEndingsWhenLimited.substr(0, std::max(0, terminalColumns - 7));
        firstSpacer = false;
    }
    if (m_opts->m_total) {
        if (!m_opts->m_wide) {
            --spacers;
            output << (!firstSpacer ? std::string(multispacers && !spacers ? lastColumnSpacer : columnSpacer, ' ') : "");
            firstSpacer = false;
        }
        (m_opts->m_wide ? totalOutput : output) << "Total [" << totalBar << "]" << wideLineEndingsWhenLimited.substr(0, std::max(0, terminalColumns - 8));
    }
    if (m_opts->m_lohi) {
        if (!m_opts->m_wide) {
            --spacers;
            output << (!firstSpacer ? std::string(multispacers && !spacers ? lastColumnSpacer : columnSpacer, ' ') : "");
            firstSpacer = false;
        }
        (m_opts->m_wide ? lohiOutput : output) << "Lohi [" << lohiBar << "]" << wideLineEndingsWhenLimited.substr(0, std::max(0, terminalColumns - 7));
    }
//		output << "\r" << std::endl;
    if (m_opts->m_verbosity) {
        int lines = VERBOSITY_NORMAL == m_opts->m_verbosity ? 1 : 5;
        for (int i = 0; i < lines; ++i) {
            spacers = dataColumns - 1;
            firstSpacer = true;
            if (memory) {
                std::string memString = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_MEMORY, m_opts->m_color, *m_colorConfig, m_opts, i);
                std::string::size_type memStringLen = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_MEMORY, false, *m_colorConfig, m_opts, i).size();
                if (m_maxOutputWidth < static_cast<int>(memStringLen)) {
                    throw std::exception();
                }
                output << memString << wideLineEndingsWhenLimited.substr(0, std::max(static_cast<std::string::size_type>(0), terminalColumns - memStringLen));
                firstSpacer = false;
            }
            if (memory && swap && !m_opts->m_wide) {
                --spacers;
                output << std::string(columnSpacer, ' ');
//					output << std::string(floor((m_terminalColumns - dataColumns * m_width) / (dataColumns - 1)), ' ');
            }
            if (swap) {
                std::string swapString = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_SWAP, m_opts->m_color, *m_colorConfig, m_opts, i);
                std::string::size_type swapStringLen = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_SWAP, false, *m_colorConfig, m_opts, i).size();
                (m_opts->m_wide ? swapOutput : output) << swapString << wideLineEndingsWhenLimited.substr(0, std::max(static_cast<std::string::size_type>(0), terminalColumns - swapStringLen));
                firstSpacer = false;
            }
            if (m_opts->m_total) {
                if (!m_opts->m_wide) {
                    --spacers;
                    output << (!firstSpacer ? std::string(multispacers && !spacers ? lastColumnSpacer : columnSpacer, ' ') : "");
                    firstSpacer = false;
//						output << std::string(m_terminalColumns - 3 * m_width - floor((m_terminalColumns - dataColumns * m_width) / (dataColumns - 1)), ' ');
                }
                std::string totalString = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_TOTAL, m_opts->m_color, *m_colorConfig, m_opts, i);
                std::string::size_type totalStringLen = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_TOTAL, false, *m_colorConfig, m_opts, i).size();
                (m_opts->m_wide ? totalOutput : output) << totalString << wideLineEndingsWhenLimited.substr(0, std::max(static_cast<std::string::size_type>(0), terminalColumns - totalStringLen));
            }
            if (m_opts->m_lohi) {
                if (!m_opts->m_wide) {
                    --spacers;
                    output << (!firstSpacer ? std::string(multispacers && !spacers ? lastColumnSpacer : columnSpacer, ' ') : "");
                    firstSpacer = false;
                }
                std::string lohiString = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_LOHI, m_opts->m_color, *m_colorConfig, m_opts, i);
                std::string::size_type lohiStringLen = sampleMemory.getText(m_maxOutputWidth, MemoryRepresentation::MODE_LOHI, false, *m_colorConfig, m_opts, i).size();
                (m_opts->m_wide ? lohiOutput : output) << lohiString << wideLineEndingsWhenLimited.substr(0, std::max(static_cast<std::string::size_type>(0), terminalColumns - lohiStringLen));
            }
        }
    }
    if (swap && m_opts->m_wide) {
        output << swapOutput.str();
    }
    if (m_opts->m_total && m_opts->m_wide) {
        output << totalOutput.str();
    }
    if (m_opts->m_lohi && m_opts->m_wide) {
        output << lohiOutput.str();
    }

    // Create string from output, ready for output to std::cout
    return output.str();
}


PaintLoop::PaintLoop(CmdLineOpts* options, ColorConfig* colorConfig): m_opts(options), m_colorConfig(colorConfig), m_maxSamples(options->m_maxSamples) {
    // set sample count to -1 to loop forever if needed
    if (!m_maxSamples) {
        --m_sampleCounter;
    }
    // Make room for output 
    if (!broken && (m_sampleCounter < m_maxSamples || -1 == m_maxSamples)) {
        std::cout << std::string(m_opts->height(), '\n') << std::flush;
    }

    /* set the terminal to raw mode */
    tcgetattr(fileno(stdin), &m_orig_term_attr);
    memcpy(&m_new_term_attr, &m_orig_term_attr, sizeof(struct termios));
    m_new_term_attr.c_lflag &= ~(ECHO|ICANON);
    m_orig_term_attr.c_lflag |= ECHO|ICANON;
    tcsetattr(fileno(stdin), TCSANOW, &m_new_term_attr);

}	

void PaintLoop::start() {
    while ((m_sampleCounter < m_maxSamples || -1 == m_maxSamples) && !broken) {
        // Calculate width and cols if needed
        int tmpTerminalColumns = m_terminalColumns;
        int dataColumns = m_opts->columns();
        if (m_calculateWidth || m_resized) {
            m_colorConfig->refresh();
            m_resized = false;
            tmpTerminalColumns = m_terminalColumns = getCols();
            m_calculateWidth = false;
        }

        // Handle no memory selected error
        if (!dataColumns) {
            int height = m_opts->height();
            std::string cls = std::string("\r\e[A") + std::string(m_terminalColumns, ' ');
            std::string error = "No memory selected."; // @TODO: i18n
            std::cout << repeat(height, "\r\e[A") << error << std::string(m_terminalColumns - error.length(), ' ') << std::endl; // @TODO: use a mb-safe length calculation based counting visible glyhps with mbrlen instead of .length() -- which only gives byte length and not visual glyph length
            if (1 == m_maxSamples ) {
                break;
            }
            pthread_cond_wait(&condition, &lock);
            continue;
        }

        // @TODO: Only perform this when data columns, verbosity or terminal width has changed
        m_width = floor((m_terminalColumns - (dataColumns - 1)) / dataColumns);
        if (m_width < MINCOLUMNWIDTH && !m_opts->m_wide) {
            // @TODO: Move output clearing code to its own function
    		std::string cls = std::string("\r\e[A") + std::string(getCols(), ' ');
            std::cout << repeat(m_opts->height(), cls);
            m_opts->m_wide = true;
            m_opts->m_temporaryWide = true;
            // @TODO: Move output clearing code to its own function
            std::cout << std::string(m_opts->height(), '\n') << std::flush;
            m_calculateWidth = true;
            m_resized = true;
            pthread_cond_wait(&condition, &lock);
            continue;
        }
        if (m_width >= MINCOLUMNWIDTH && m_opts->m_temporaryWide && !m_caughtException) {
            // @TODO: Move output clearing code to its own function
    		std::string cls = std::string("\r\e[A") + std::string(getCols(), ' ');
            std::cout << repeat(m_opts->height(), cls);
            m_opts->m_temporaryWide = false;
            m_opts->m_wide = false;
            // @TODO: Move output clearing code to its own function
            std::cout << std::string(m_opts->height(), '\n') << std::flush;
            m_calculateWidth = true;
            m_resized = true;
            pthread_cond_wait(&condition, &lock);
            continue;
        }
        m_maxOutputWidth = std::min(m_opts->m_wide ? m_terminalColumns : m_width, m_opts->m_maxWidth);

        MemoryRepresentation sampleMemory;
        std::string outputString;
        bool previouslyWide = m_opts->m_wide;
        if (m_caughtException && m_opts->m_temporaryWide && m_opts->m_wide && m_width >= MINCOLUMNWIDTH) {
            try {
                m_opts->m_wide = false;
                m_opts->m_temporaryWide = false;
                m_caughtException = false;
                m_maxOutputWidth = std::min(m_opts->m_wide ? m_terminalColumns : m_width, m_opts->m_maxWidth);
                outputString = getOutput(sampleMemory, m_terminalColumns, dataColumns);
                // @TODO: Move output clearing code to its own function
                m_opts->m_wide = previouslyWide;
                std::string cls = std::string("\r\e[A") + std::string(getCols(), ' ');
                std::cout << repeat(m_opts->height(), cls);
                m_opts->m_wide = false;
                // @TODO: Move output clearing code to its own function
                std::cout << std::string(m_opts->height(), '\n') << std::flush;
            } catch (std::exception const & exception) {
                // @TODO: Move output clearing code to its own function
                m_opts->m_wide = previouslyWide;
                std::string cls = std::string("\r\e[A") + std::string(getCols(), ' ');
                std::cout << repeat(m_opts->height(), cls);
                m_opts->m_wide = true;
                m_opts->m_temporaryWide = true;
                m_caughtException = true;
                m_maxOutputWidth = std::min(m_opts->m_wide ? m_terminalColumns : m_width, m_opts->m_maxWidth);
                // @TODO: Move output clearing code to its own function
                std::cout << std::string(m_opts->height(), '\n') << std::flush;
                try {
                    outputString = getOutput(sampleMemory, m_terminalColumns, dataColumns);
                } catch (std::exception const & innerException) {
                    m_calculateWidth = true;
                    m_resized = true;
                    pthread_cond_wait(&condition, &lock);
                    continue;
                }
            }
        } else {
            try {
                outputString = getOutput(sampleMemory, m_terminalColumns - 1, dataColumns);
            } catch (std::exception const & exception) {
                // @TODO: Move output clearing code to its own function
                std::string cls = std::string("\r\e[A") + std::string(getCols(), ' ');
                std::cout << repeat(m_opts->height(), cls);
                m_opts->m_wide = true;
                m_opts->m_temporaryWide = true;
                m_caughtException = true;
                // @TODO: Move output clearing code to its own function
                std::cout << std::string(m_opts->height(), '\n') << std::flush;
                m_calculateWidth = true;
                m_resized = true;
                continue;
            }
        }

        // @TODO: Don't refresh color config, but rely on an inotify to make sure the current config is always uptodate
        m_colorConfig->refresh();
        tmpTerminalColumns = getCols(); // @TODO: Also check if verbosity/output length has changed, etc.
        if (tmpTerminalColumns == m_terminalColumns && !m_resized) { // Terminal width hasn't changed during rendering, ok to print it to std::cout
            std::cout << outputString << std::endl;
        }
        if (m_resized) {
            m_calculateWidth = true;
        }

        // increment sample count if there is a max sample count
        if (m_maxSamples && !m_calculateWidth) {
            ++m_sampleCounter;
        }

        // Exit if only one sample is supposed to be printed
        if (1 == m_maxSamples) {
            break;
        }

        // Exit if exiting state has been set
        if (broken) {
            tcsetattr(fileno(stdin), TCSANOW, &m_orig_term_attr);
            exit(0);
            break;
        }

        // Wait until next thread interuption (by timer, signal or keyboard input)
        pthread_cond_wait(&condition, &lock);
    }

    /* restore the original terminal attributes */
    tcsetattr(fileno(stdin), TCSANOW, &m_orig_term_attr);

}

void PaintLoop::resize() {
    m_resized = true;
    pthread_cond_signal(&condition);
}

void PaintLoop::refresh() {
    pthread_cond_signal(&condition);
}

void PaintLoop::stop() {
    broken = true;
    pthread_cond_signal(&condition);
}

void PaintLoop::paus() {

}

void PaintLoop::resume() {

}
