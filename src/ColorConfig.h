#ifndef COLORCONFIG_H
#define COLORCONFIG_H

#include <string>
#include <vector>

class ColorConfig {
private:
	std::string m_configFile;
	int m_timestamp;
	std::string m_colorCode[13];
	bool saveConfig(std::string key, std::string value);
	bool fileExists(std::string filename);
	std::string getConfigFile();

public:
	ColorConfig();
	static const int COLOR_SUMMARY = 0;
	static const int COLOR_NORMAL = 1;
	static const int COLOR_WARNING = 2;
	static const int COLOR_ALERT = 3;
	static const int COLOR_BUFFER = 4;
	static const int COLOR_CACHED = 5;
	static const int COLOR_LO_TOTAL = 6;
	static const int COLOR_HI_TOTAL = 7;
	static const int COLOR_LO_USED = 8;
	static const int COLOR_HI_USED = 9;
	static const int COLOR_LO_FREE = 10;
	static const int COLOR_HI_FREE = 11;
	
	static const int COLOR_FLAGS = 12;

	bool refresh();
	std::string getAnsiCode(int color) const;
	std::vector<const char*> flags() const;
};

#endif
