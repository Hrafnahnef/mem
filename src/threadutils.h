#ifndef THREADUTILS_H
#define THREADUTILS_H

#include <pthread.h>

static pthread_cond_t condition = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static bool resized = false;
static bool broken = false;

static inline void quit() { broken = true; pthread_cond_signal(&condition); }
static inline void revalidateSize() { resized = true; pthread_cond_signal(&condition); }

void wait_till_next_tick(int decitick);

#endif
