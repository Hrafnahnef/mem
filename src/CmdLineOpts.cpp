#include "CmdLineOpts.h"

#include "threadutils.h"
#include "term.h"
#include "version.h"

#include <iomanip>
#include <ios>
#include <iostream>
#include <sstream>
#include <sys/eventfd.h>
#include <termcap.h>
#include <unistd.h>

#define term_buffer 0

void CmdLineOpts::_init() {
    m_lohi = false;
    m_show = SHOW_BOTH;
    m_total = false;
    m_wide = false;
    m_frog = false;
    m_verbosity = VERBOSITY_NORMAL;
    m_maxWidth = std::numeric_limits<int>::max();
    m_delay = 10;
    m_unit = -1024L;
    m_maxSamples = 1;

    /* Autodetect color support */
    m_color = true;
    char *termtype = getenv("TERM");
    if (termtype == 0) {
        m_color = false;
    }
    int success = tgetent(term_buffer, termtype);
    m_color = success <= 0 ? false : tgetnum("colors") >= 8;
    if (!isatty(fileno(stdout))) {
        m_color = false;
    }
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            printFrog();
            quit();
            exit(0);
        },
        "",
        {"frog", "mrfroggy", "froggy", "froggies"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_delay = 10 * atoi(value.c_str());
            options->incrementTokenizer();
            // @TODO: Consume value
        },
        "s",
        {"sec", "secs", "second", "seconds"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_delay = atoi(value.c_str());
            options->incrementTokenizer();
            // @TODO: Consume value
        },
        "d",
        {"delay", "decisecond", "deciseconds", "deci-second", "deci-seconds"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            if ("brief" == value || "0" == value || "disable" == value) {
                options->m_verbosity = VERBOSITY_BRIEF;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else if ("normal" == value || "1" == value) {
                options->m_verbosity = VERBOSITY_NORMAL;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else if ("verbose" == value || "2" == value || "enable" == value) {
                options->m_verbosity = VERBOSITY_VERBOSE;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else {
                options->m_verbosity = VERBOSITY_VERBOSE;
            }
        },
        "v",
        {"verbose", "verbosity"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_verbosity = VERBOSITY_BRIEF;
        },
        "B",
        {"brief"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_verbosity = VERBOSITY_BRIEF;
        },
        "B",
        {"brief"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->printLicense();
            quit();
            exit(0);
        },
        "",
        {"license"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->printAuthors();
            quit();
            exit(0);
        },
        "a",
        {"author", "authors"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->printVersion();
            options->printUsage();
            options->printHelp();
            quit();
            exit(0);
        },
        "h",
        {"help", "info"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->printUsage();
            quit();
            exit(0);
        },
        "u",
        {"usage", "tldr"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->printVersion();
            quit();
            exit(0);
        },
        "V",
        {"version"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            if ("no" == value || "false" == value || "0" == value || "disable" == value) {
                options->m_lohi = false;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else if ("yes" == value || "false" == value || "1" == value || "enable" == value) {
                options->m_lohi = true;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else {
                options->m_lohi = true;
            }
        },
        "l",
        {"lohi", "lowhigh", "lohigh", "lowhi", "lo-hi", "low-high", "low-hi", "lo-high"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1L;
        },
        "b",
        {"byte", "bytes"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1000L;
        },
        "K",
        {"kb", "kilo", "kilobyte", "kilobytes", "kilo-byte", "kilo-bytes"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1024L;
        },
        "k",
        {"kib", "kibi", "kibibyte", "kibibytes", "kilo-binary", "kilobinary", "kibi-bytes", "kibi-byte"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1000L * 1000L;
        },
        "M",
        {"mb", "mega", "megabyte", "megabytes", "mega-byte", "mega-bytes"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1024L * 1024L;
        },
        "m",
        {"mib", "mebi", "mebibyte", "mebibytes", "mega-binary", "megabinary"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1000L * 1000L * 1000L;
        },
        "G",
        {"gb", "giga", "gigabyte", "gigabytes", "giga-byte", "giga-bytes"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1024L * 1024L * 1024L;
        },
        "g",
        {"gib", "gibi", "gibibyte", "gibibytes", "giga-binary", "gigabinary"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1000L * 1000L * 1000L * 1000L;
        },
        "T",
        {"tb", "tera", "terabyte", "terabytes", "tera-byte", "tera-bytes"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1024L * 1024L * 1024L * 1024L;
        },
        "t",
        {"tib", "tibi", "tibibyte", "tibibytes", "tera-binary", "terabinary"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1000L * 1000L * 1000L * 1000L * 1000L;
        },
        "P",
        {"pb", "peta", "petabyte", "petabytes", "peta-byte", "peta-bytes"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = 1024L * 1024L * 1024L * 1024L * 1024L;
        },
        "p",
        {"pib", "pebi", "pebibyte", "pebibytes", "peta-binary", "petabinary"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = -1000L;
        },
        "H",
        {"si", "sihumanreadable", "si-humanreadable", "si-human-readable", "sihuman-readable"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_unit = -1024L;
        },
        "",
        {"human", "human-readable", "humanreadable", "traditional"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_color = false;
        },
        "n",
        {"nocolor", "no-color", "no-colors", "nocolors", "nocolour", "no-colour", "no-colours", "nocolours"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            if ("yes" == value || "1" == value || "force" == value || "enable" == value) {
                options->m_color = true;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else if ("no" == value || "0" == value || "disable" == value || "prevent" == value) {
                options->m_color = false;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else {
                options->m_color = true;
            }
        },
        "C",
        {"color", "colour"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            if ("yes" == value || "1" == value || "force" == value || "enable" == value) {
                options->m_wide = true;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else if ("no" == value || "0" == value || "disable" == value || "prevent" == value) {
                options->m_wide = false;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else {
                options->m_wide = true;
            }
        },
        "w",
        {"wide", "wide-output", "wideoutput", "fullwidth", "full-width"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_maxWidth = atoi(value.c_str());
            options->incrementTokenizer();
        },
        "x",
        {"max", "maxwidth", "width", "max-width"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_show |= SHOW_MEMORY;
        },
        "y",
        {"mem", "memory"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_show = SHOW_MEMORY;
        },
        "",
        {"only-memory", "only-mem", "onlymem", "onlymemory"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_show |= SHOW_SWAP;
        },
        "",
        {"swap"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_show = SHOW_SWAP;
        },
        "",
        {"only-swap", "onlyswap"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_show &= ~SHOW_MEMORY;
        },
        "Y",
        {"nomem", "no-memory", "no-mem", "nomemory"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_show &= ~SHOW_SWAP;
        },
        "S",
        {"noswap", "no-swap"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            if ("no" == value || "false" == value || "0" == value || "disable" == value) {
                options->m_total = false;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else if ("yes" == value || "false" == value || "1" == value || "enable" == value) {
                options->m_total = true;
                options->incrementTokenizer();
                // @TODO: Consume value
            } else {
                options->m_total = true;
            }
        },
        "j",
        {"total", "merged", "join", "merge", "joined"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_maxSamples = -1;
        },
        "i",
        {"interactive", "follow", "continous"}
    ));
    registerParameter(new Argument(
        [](CmdLineOpts* options, const std::string& parameter, const std::string& value) {
            options->m_maxSamples = atoi(value.c_str());
            options->incrementTokenizer();
            // @TODO: Consume value
        },
        "c",
        {"count", "max-samples", "maxsamples", "max", "max-count", "maxcount"}
    ));
}

void CmdLineOpts::registerParameter(Argument* argument) {
    if ("" != argument->m_shortArg) {
        m_shortArgs[argument->m_shortArg] = argument;
    }
    for (std::string longArg : argument->m_longArgs) {
        m_longArgs[longArg] = argument;
    }
}

CmdLineOpts::CmdLineOpts(int argc, const char** argv) {
    _init();
    init(argc, argv, 1);
}

CmdLineOpts::CmdLineOpts(const ColorConfig& colorConfig, int argc, const char** argv) {
    _init();
	std::vector<const char*> flags(colorConfig.flags());
	init(flags.size(), &flags[0]);
	init(argc, argv, 1);
}

void CmdLineOpts::init(int argc, const char** argv, int start) {
    for (m_tokenizerWordCounter = start; m_tokenizerWordCounter < argc; ++m_tokenizerWordCounter) {
        std::string str(argv[m_tokenizerWordCounter]);
        if (str.length() > 2 && '-' == str[0] && '-' == str[1]) {
            std::string key(str.substr(2));
            std::string value = 1 + m_tokenizerWordCounter >= argc ? "" : std::string(argv[1 + m_tokenizerWordCounter]); // @TODO, get number of args from map itself (could be a struct of specification and the pointer itself)
            std::string::size_type eq = key.find('=');
            if (eq != std::string::npos) {
                value = key.substr(eq + 1);
                key = key.substr(0, eq);
                m_tokenizerWordPointer = str.cbegin() + eq;
            }
            if (m_longArgs.contains(key)) {
                m_longArgs[key]->call(this, key, value);
            }
        } else if (str.length() > 1 && '-' == str[0]) {
            std::string key(str.substr(1));
            bool longWordMatch = m_longArgs.contains(str.substr(1));
            if (longWordMatch) {/*
                for (std::string::const_iterator it = str.cbegin(); it != str.cend(); ++it) {
                    @TODO: Iterate over all utf-8 multibyte glyphs and check if it is an argument,
                    AND if it has a value, check for value at next position, until the end
                    AND if the entire string matches, go on and perform the flags, otherwise perform the longArg
                }*/
            }
            for (m_tokenizerWordPointer = str.cbegin() + 1, m_tokenizerWordEnd = str.cend(); m_tokenizerWordPointer < m_tokenizerWordEnd; ++m_tokenizerWordPointer) {
                std::string key(1, *m_tokenizerWordPointer);
                if (m_shortArgs.contains(key)) {
                    // @TODO: Make sure numericals equal signs, quotation marks, apostrophes and string such as yes|no are kept together
                    std::string value = "";
                    if ((1 + m_tokenizerWordPointer) < m_tokenizerWordEnd) {
                        if (*(1+ m_tokenizerWordPointer) >= '0' && *(1 + m_tokenizerWordPointer) <= '9') {
                            int i = 1;
                            while ((m_tokenizerWordPointer + i) < m_tokenizerWordEnd && *(m_tokenizerWordPointer + i) >= '0' && *(m_tokenizerWordPointer + i) <= '9') {
                                value += std::string(1, *(i+m_tokenizerWordPointer));
                                ++i;
                            }
                        } else {
                            value = std::string(1, *(1+m_tokenizerWordPointer));
                        }
                    } else if ((1 + m_tokenizerWordCounter) < argc) {
                        value = std::string(argv[1 + m_tokenizerWordCounter]);
                    }
                    m_shortArgs[key]->call(this, key, value);
                }
            }
            m_tokenizerWordPointer = static_cast<std::string::const_iterator>(0);
            m_tokenizerWordEnd = static_cast<std::string::const_iterator>(0);
        } else {
            std::string value = 1 + m_tokenizerWordCounter >= argc ? "" : std::string(argv[1 + m_tokenizerWordCounter]); // @TODO, get number of args from map itself (could be a struct of specification and the pointer itself)
            std::string key(str);
            std::string::size_type eq = key.find('=');
            if (eq != std::string::npos) {
                value = key.substr(eq + 1);
                key = key.substr(0, eq);
                m_tokenizerWordPointer = str.cbegin() + eq;
            }
            if (m_longArgs.contains(key)) {
                m_longArgs[key]->call(this, key, value);
            } else {
                for (m_tokenizerWordPointer = str.cbegin(), m_tokenizerWordEnd = str.cend(); m_tokenizerWordPointer < m_tokenizerWordEnd; ++m_tokenizerWordPointer) {
                    std::string key(1, *m_tokenizerWordPointer);
                    if (m_shortArgs.contains(key)) {
                        // @TODO: Make sure numericals equal signs, quotation marks, apostrophes and string such as yes|no are kept together
                        std::string value = "";
                        if ((1 + m_tokenizerWordPointer) < m_tokenizerWordEnd) {
                            if (*(1+ m_tokenizerWordPointer) >= '0' && *(1 + m_tokenizerWordPointer) <= '9') {
                                int i = 1;
                                while ((m_tokenizerWordPointer + i) < m_tokenizerWordEnd && *(m_tokenizerWordPointer + i) >= '0' && *(m_tokenizerWordPointer + i) <= '9') {
                                    value += std::string(1, *(i+m_tokenizerWordPointer));
                                    ++i;
                                }
                            } else {
                                value = std::string(1, *(1+m_tokenizerWordPointer));
                            }
                        } else if ((1 + m_tokenizerWordCounter) < argc) {
                            value = std::string(argv[1 + m_tokenizerWordCounter]);
                        }
                        m_shortArgs[key]->call(this, key, value);
                    }
                }
                m_tokenizerWordPointer = static_cast<std::string::const_iterator>(0);
                m_tokenizerWordEnd = static_cast<std::string::const_iterator>(0);
            }
        }
    }
    m_tokenizerWordCounter = -1;
    m_tokenizerWordPointer = static_cast<std::string::const_iterator>(0);
    m_tokenizerWordEnd = static_cast<std::string::const_iterator>(0);
}

void CmdLineOpts::incrementTokenizer() {
    if (static_cast<std::string::const_iterator>(0) == m_tokenizerWordPointer) {
        ++m_tokenizerWordCounter;
    } else {
        if ((++m_tokenizerWordPointer) < m_tokenizerWordEnd) {
            if (*m_tokenizerWordPointer >= '0' && *m_tokenizerWordPointer <= '9') {
                while (1 + m_tokenizerWordPointer < m_tokenizerWordEnd && (*(1 + m_tokenizerWordPointer) >= '0') && (*(1 + m_tokenizerWordPointer) <= '9')) {
                    ++m_tokenizerWordPointer;
                }
            }
        } else if (m_tokenizerWordPointer >= m_tokenizerWordEnd) {
            ++m_tokenizerWordCounter;
        }
    }
}

int CmdLineOpts::height() const {
    int height = 1;
    switch (m_verbosity) {
        case VERBOSITY_NORMAL:
            ++height;
            break;
        case VERBOSITY_VERBOSE:
            height += 5;
            break;
    }
    return columns() ? height * (this->m_wide ? this->columns() : 1) : 1;
}

int CmdLineOpts::columns() const {
    int columns = 0;
    if (SHOW_MEMORY & this->m_show) {
        ++columns;
    }
    if (SHOW_SWAP & this->m_show) {
        ++columns;
    }
    if (this->m_total) {
        ++columns;
    }
    if (this->m_lohi) {
        ++columns;
    }
    return columns;
}

void CmdLineOpts::fixateHumanReadableUnit(unsigned long total) {
    if (m_unit == -1024L) {
        if (total > 10L * 1024L * 1024L * 1024L) {
            m_unit = 1024L * 1024L * 1024L * 1024L;
        } else if (total > 10L * 1024L * 1024L) {
            m_unit = 1024L * 1024L * 1024L;
        } else if (total > 10L * 1024L) {
            m_unit = 1024L * 1024L;
        } else if (total > 10L) {
            m_unit = 1024L;
        } else {
            m_unit = 1L;
        }
    } else if (m_unit == -1000L) {
        if (total > 10L * 1000L * 1000L * 1000L) {
            m_unit = 1000L * 1000L * 1000L * 1000L;
        } else if (total > 10L * 1000L * 1000L) {
            m_unit = 1000L * 1000L * 1000L;
        } else if (total > 10L * 1000L) {
            m_unit = 1000L * 1000L;
        } else if (total > 10L) {
            m_unit = 1000L;
        } else {
            m_unit = 1L;
        }
    }
}

CmdLineOpts::~CmdLineOpts() {}

void CmdLineOpts::printAuthors() const {
    const unsigned short int firstColumnWidth = 4;
    std::cout << "Authors:" << std::endl;
    std::cout << std::string(firstColumnWidth, ' ') << "The mem team" << std::endl;
}

void CmdLineOpts::printHelp() const {
    const unsigned short int firstColumnWidth = 8, secondColumnWidth = 16;
    std::ios_base::fmtflags reset(std::cout.flags());
    const std::string padding = std::string(4, ' ');

    std::cout << padding << std::left << std::setw(firstColumnWidth) << "-h" << std::setw(secondColumnWidth) <<  std::left <<  "--help"/* << reset*/ << "Print this help and quit." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) << std::left << "-u" << std::setw(secondColumnWidth) <<  std::left <<  "--usage"/* << reset*/ << "Print usage and quit." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-V" << std::setw(secondColumnWidth) <<  std::left <<  "--version"/* << reset*/ << "Print version and quit." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-L" << std::setw(secondColumnWidth) <<  std::left <<  "--license"/* << reset*/ << "Print license and quit." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-a" << std::setw(secondColumnWidth) <<  std::left <<  "--author"/* << reset*/ << "Print license and quit." << std::endl;
    std::cout << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-B" << std::setw(secondColumnWidth) <<  std::left <<  "--brief"/* << reset*/ << "Be more brief." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "" << std::setw(secondColumnWidth) <<  std::left <<  "--verbosity=normal"/* << reset*/ << "Normal verbosity. (Default)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-v" << std::setw(secondColumnWidth) <<  std::left <<  "--verbose"/* << reset*/ << "Be more verbose." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-w" << std::setw(secondColumnWidth) <<  std::left <<  "--wide"/* << reset*/ << "Print each data entry sequentially in full width." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-x N" << std::setw(secondColumnWidth) <<  std::left <<  "--max-width N"/* << reset*/ << "Limit output width of a data entry to N characters." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-S" << std::setw(secondColumnWidth) <<  std::left <<  "--no-swap"/* << reset*/ << "Don't print swap. Only print memory usage." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-Y" << std::setw(secondColumnWidth) <<  std::left <<  "--no-mem"/* << reset*/ << "Don't print memory. Only print swap usage." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << ""   << std::setw(secondColumnWidth) <<  std::left <<  "--swap"/* << reset*/ << "Print swap if available. (Default)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-y" << std::setw(secondColumnWidth) <<  std::left <<  "--mem"/* << reset*/ << "Print memory. (Default)" << std::endl;
    std::cout << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-n" << std::setw(secondColumnWidth) <<  std::left <<  "--no-color"/* << reset*/ << "Don't use ANSI colored output." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-C" << std::setw(secondColumnWidth) <<  std::left <<  "--color"/* << reset*/ << "Force use of ANSI colored output." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-c N" << std::setw(secondColumnWidth) <<  std::left <<  "--count N"/* << reset*/ << "Only print N samples, then quit. 0 (default) means never quit, same as interactive mode." << std::endl;
//		std::cout << std::setw(firstColumnWidth) <<  std::left << "-N"/* << reset*/ << "Shorthand of -c N or --count N, e.g. -5 or -200" << std::endl; 
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-i" << std::setw(secondColumnWidth) <<  std::left <<  "--interactive"/* << reset*/ << "Enter interactive mode, print memory until stopped by 'q' keypress." << std::endl;
    std::cout << std::endl;
    std::cout << "Specifying polling time. These arguments are mutuably exclusive. The last one defined takes precedence." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-s N" << std::setw(secondColumnWidth) <<  std::left <<  "--seconds N"/* << reset*/ << "Delay N seconds between every sample of memory. Defaults to 1." << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-d N" << std::setw(secondColumnWidth) <<  std::left <<  "--delay N"/* << reset*/ << "Delay N deciseconds (tenths of a second) between every sample of memory. Defaults to 10 (1 second)." << std::endl;
    std::cout << std::endl;
    std::cout << "Output units:" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-b" << std::setw(secondColumnWidth) <<  std::left <<  "--bytes"/* << reset*/ << "Print all output in bytes" << std::endl;	
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-k" << std::setw(secondColumnWidth) <<  std::left <<  "--kilo"/* << reset*/ << "Print all output in SI kilo units (1000 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-K" << std::setw(secondColumnWidth) <<  std::left <<  "--kibi"/* << reset*/ << "Print all output in traditional KiBi units (1024 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-m" << std::setw(secondColumnWidth) <<  std::left <<  "--mega"/* << reset*/ << "Print all output in SI mega units (1000^2 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-M" << std::setw(secondColumnWidth) <<  std::left <<  "--mebi"/* << reset*/ << "Print all output in traditional MeBi units (1024^2 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-g" << std::setw(secondColumnWidth) <<  std::left <<  "--giga"/* << reset*/ << "Print all output in SI giga units (1000^3 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-G" << std::setw(secondColumnWidth) <<  std::left <<  "--gibi"/* << reset*/ << "Print all output in traditional GiBi units (1024^3 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-t" << std::setw(secondColumnWidth) <<  std::left <<  "--tera"/* << reset*/ << "Print all output in SI tera units (1000^4 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-T" << std::setw(secondColumnWidth) <<  std::left <<  "--tebi"/* << reset*/ << "Print all output in traditional TeBi units (1024^4 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-p" << std::setw(secondColumnWidth) <<  std::left <<  "--peta"/* << reset*/ << "Print all output in SI peta units (1000^5 bytes)" << std::endl;
    std::cout << padding << std::setw(firstColumnWidth) <<  std::left << "-P" << std::setw(secondColumnWidth) <<  std::left <<  "--pebi"/* << reset*/ << "Print all output in traditional PeBi units (1024^5 bytes)" << std::endl;
    std::cout << std::endl;
    std::cout << "Interactive mode:" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "Press any of these keys:" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<q> or <ESC>"/* << reset*/ << "Quit" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<s>"/* << reset*/ << "Toggle swap" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<y>"/* << reset*/ << "Toggle mem" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<j>"/* << reset*/ << "Toggle joined/total" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<w>"/* << reset*/ << "Toggle full width mode" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<b>"/* << reset*/ << "Switch all output to display in bytes" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<k|m|g|t|p>"/* << reset*/ << "Switch all output to display in SI kilo, mega, giga, tera and peta units" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<K|M|G|T|P>"/* << reset*/ << "Switch all output to display in traditional KiBi, MeBi, GiBi, TeBi and PeBi units" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<h>"/* << reset*/ << "Switch all output to display in automatically detected SI units" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<H>"/* << reset*/ << "Switch all output to display in automatically detected traditional units" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<v> or <V>"/* << reset*/ << "Cycle Verbosity up or down" << std::endl;
    std::cout << padding << std::setw(secondColumnWidth) <<  std::left << "<r>"/* << reset*/ << "Refresh, even if delay specified in -d N or -s N have not elapsed yet" << std::endl;
}

void CmdLineOpts::printLicense() const {
    const unsigned short int firstColumnWidth = 4;
    std::cout << "License:" << std::endl;
    // @TODO: Line break paragraphs after min(getCols(), 80), according to visible unicode glyphs
    std::cout << "Copyright (c) 2014-, Aron Cederholm" << std::endl;
    std::cout << "Redistribution and use in source and binary forms, with or without" << std::endl;
    std::cout << "modification, are permitted provided that the following conditions are met:" << std::endl << std::endl;
    std::cout << "1. Redistributions of source code must retain the above copyright notice," << std::endl;
    std::cout << "this list of conditions and the following disclaimer." << std::endl << std::endl;
    std::cout << "2. Redistributions in binary form must reproduce the above copyright notice," << std::endl;
    std::cout << "this list of conditions and the following disclaimer in the documentation" << std::endl;
    std::cout << "and/or other materials provided with the distribution." << std::endl << std::endl;
    std::cout << "3. Neither the name of the copyright holder nor the names of its contributors" << std::endl;
    std::cout << "may be used to endorse or promote products derived from this software without" << std::endl;
    std::cout << "specific prior written permission." << std::endl << std::endl;
    std::cout << "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"" << std::endl;
    std::cout << "AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE" << std::endl;
    std::cout << "IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE" << std::endl;
    std::cout << "ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE" << std::endl;
    std::cout << "LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CON-" << std::endl;
    std::cout << "SEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE" << std::endl;
    std::cout << "GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)" << std::endl;
    std::cout << "HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT" << std::endl;
    std::cout << "LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT" << std::endl;
    std::cout << "OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF" << std::endl;
    std::cout << "SUCH DAMAGE." << std::endl;
}

void CmdLineOpts::printUsage() const {
    const unsigned short int firstColumnWidth = 4;
    std::cout << "Usage:" << std::endl;
    std::cout << std::string(firstColumnWidth, ' ') << "mem [OPTIONS]" << std::endl;
}

void CmdLineOpts::printVersion() const {
    std::cout << "mem " << MEM_VERSION << std::endl;
}
