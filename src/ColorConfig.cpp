#include "ColorConfig.h"
#include "strutils.h"

#include <algorithm>
#include <fstream>
#include <iterator>
#include <sstream>
#include <sys/stat.h>

bool ColorConfig::saveConfig(std::string key, std::string value) {
    if (key == "summary") {
        m_colorCode[COLOR_SUMMARY]   = "\e[" + value + "m";
        return true;
    } else if (key == "normal") {
        m_colorCode[COLOR_NORMAL]    = "\e[" + value + "m";
        return true;
    } else if (key == "warning") {
        m_colorCode[COLOR_WARNING]   = "\e[" + value + "m";
        return true;
    } else if (key == "alert") {
        m_colorCode[COLOR_ALERT]     = "\e[" + value + "m";
        return true;
    } else if (key == "buffer") {
        m_colorCode[COLOR_BUFFER]    = "\e[" + value + "m";
        return true;
    } else if (key == "cached") {
        m_colorCode[COLOR_CACHED]    = "\e[" + value + "m";
        return true;
    } else if (key == "high-total") {
        m_colorCode[COLOR_HI_TOTAL] = "\e[" + value + "m";
        return true;
    } else if (key == "low-total") {
        m_colorCode[COLOR_LO_TOTAL] = "\e[" + value + "m";
        return true;
    } else if (key == "high-used") {
        m_colorCode[COLOR_HI_USED] = "\e[" + value + "m";
        return true;
    } else if (key == "low-used") {
        m_colorCode[COLOR_LO_USED] = "\e[" + value + "m";
        return true;
    } else if (key == "high-free") {
        m_colorCode[COLOR_HI_FREE] = "\e[" + value + "m";
        return true;
    } else if (key == "low-free") {
        m_colorCode[COLOR_LO_FREE] = "\e[" + value + "m";
        return true;
    } else if (key == "flags") {
        m_colorCode[COLOR_FLAGS]     = value;
        return true;
    }
    return false;
}

bool ColorConfig::fileExists(std::string filename) {
    struct stat buffer;
    return 0 == stat(filename.c_str(), &buffer);
}

std::string ColorConfig::getConfigFile() {
    std::vector<std::string> searchPaths;
    std::string xdgPath(getenv("XDG_CONFIG_HOME"));
    std::string homePath(getenv("HOME"));
    if ("" != xdgPath) {
        searchPaths.push_back(xdgPath + std::string("/memrc"));
    }
    if ("" != homePath) {
        searchPaths.push_back(homePath + std::string("/.config/memrc"));
        searchPaths.push_back(homePath + std::string("/.memrc"));
    }
    searchPaths.push_back("/etc/mem/memrc");
    for (std::vector<std::string>::const_iterator it = searchPaths.cbegin(); it != searchPaths.cend(); ++it) {
        if (this->fileExists(*it)) {
            return *it;
        }
    }
    return "";
}

ColorConfig::ColorConfig() {
    m_colorCode[COLOR_SUMMARY]   = "\e[1;35m";
    m_colorCode[COLOR_NORMAL]    = "\e[0;32m";
    m_colorCode[COLOR_WARNING]   = "\e[0;31m";
    m_colorCode[COLOR_ALERT]     = "\e[1;31m";
    m_colorCode[COLOR_BUFFER]    = "\e[1;33m";
    m_colorCode[COLOR_CACHED]    = "\e[0;34m";
    m_colorCode[COLOR_LO_TOTAL]  = "\e[1;35m";
    m_colorCode[COLOR_HI_TOTAL]  = "\e[1;35m";
    m_colorCode[COLOR_LO_USED]   = "\e[0;32m";
    m_colorCode[COLOR_HI_USED]   = "\e[0;32m";
    m_colorCode[COLOR_LO_FREE]   = "\e[1;35m";
    m_colorCode[COLOR_HI_FREE]   = "\e[1;35m";
    m_colorCode[COLOR_FLAGS]     = "";
    m_configFile = this->getConfigFile();
    m_timestamp = 0;
    refresh();
}

bool ColorConfig::refresh() {
    if (m_configFile == "") {
        return false;
    }
    std::ifstream file(m_configFile.c_str());
    if (!file) {
        return false;
    }
    struct stat st;
    int err = stat(m_configFile.c_str(), &st);
    if (st.st_mtime > m_timestamp) {
        m_timestamp = st.st_mtime;
    } else {
        return false;
    }
    std::string line;
    while (getline(file, line)) {
        std::istringstream iss_line(line);
        std::string key;
        if (getline(iss_line, key, '=')) {
            rtrim(key);
            std::string value;
            if (getline(iss_line, value)) {
                trim(value);
                saveConfig(key, value);
            }
        }
    }
    return true;
}

std::string ColorConfig::getAnsiCode(int color) const {
    return m_colorCode[color];
}

std::vector<const char*> ColorConfig::flags() const {
	std::vector<const char*> result;
	std::string flagsString(m_colorCode[COLOR_FLAGS]);
	std::istringstream flagsStream(flagsString);
	std::vector<std::string> flagsStringVector(std::istream_iterator<std::string>{flagsStream}, std::istream_iterator<std::string>{});
	std::transform(flagsStringVector.cbegin(), flagsStringVector.cend(), back_inserter(result), convert);
    return result;
}