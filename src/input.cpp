#include "input.h"

#include "CmdLineOpts.h"
#include "PaintLoop.h"
#include "strutils.h"
#include "term.h"
#include "threadutils.h"

#include <iostream>
#include <pthread.h>
#include <sstream>
#include <string>

void* pollingLoop(void *arg) {
	PaintLoop* painter((PaintLoop*) arg);
    CmdLineOpts* options(painter->m_opts);
	while (!broken) {
		wait_till_next_tick(options->m_delay);
		painter->refresh();
	}
	return NULL;
}

void* keyboardEventLoop(void *arg) {
	PaintLoop* painter((PaintLoop*) arg);
    CmdLineOpts* options(painter->m_opts);
	while (!broken) {
		int character;

		/* read a character from the stdin stream without blocking */
		/*   returns EOF (-1) if no character is available */
		character = fgetc(stdin);

		std::string cls = std::string("\r\e[A") + std::string(getCols(), ' ');
		switch (character) {
			case 0x1B: case 0x04: case 'q': case 'c': case 'Q': case 'C':
				painter->stop();
                break;
			case 'h': case 'H': case 'b': case 'k': case 'K': case 'm': case 'M': case 'g': case 'G': case 't': case 'T': case 'p': case 'P': {
				// make sure the paint loop is locked
					std::string arg = std::string({'-', (char)character});
					const char* argv[1] = {arg.c_str()};
					if ('h' == character) {
						options->m_unit = -1024L;
					} else {
						options->init(1, (const char**)argv);
					}
					painter->refresh();
				}
				break;
			case 's': case 'S':
				// make sure the paint loop is locked
				std::cout << repeat(options->height(), cls);
				resized = true;
				options->m_show ^= SHOW_SWAP;
				std::cout << std::string(options->height(), '\n') << std::flush;
				painter->refresh();
				break;
			case 'y': case 'Y':
				// make sure the paint loop is locked
				std::cout << repeat(options->height(), cls);
				resized = true;
				options->m_show ^= SHOW_MEMORY;
				std::cout << std::string(options->height(), '\n') << std::flush;
				painter->refresh();
				break;
			case 'v': {
				// make sure the paint loop is locked
				std::cout << repeat(options->height(), cls);
				switch (options->m_verbosity) {
					case VERBOSITY_BRIEF:
						++options->m_verbosity;
						break;
					case VERBOSITY_NORMAL:
						++options->m_verbosity;
						break;
					case VERBOSITY_VERBOSE:
						options->m_verbosity = VERBOSITY_BRIEF;
						break;
				}
				std::cout << std::string(options->height(), '\n') << std::flush;
				painter->refresh();
				break;
			}
			case 'V': {
				// make sure the paint loop is locked
				std::cout << repeat(options->height(), cls);
				switch (options->m_verbosity) {
					case VERBOSITY_BRIEF:
						options->m_verbosity = VERBOSITY_VERBOSE;
						break;
					case VERBOSITY_NORMAL:
						--options->m_verbosity;
						break;
					case VERBOSITY_VERBOSE:
						--options->m_verbosity;
						break;
				}
				std::cout << std::string(options->height(), '\n') << std::flush;
				painter->refresh();
				break;
			}
			case 'w': {
				// make sure the paint loop is locked
				std::cout << repeat(options->height(), cls);
				options->m_wide = !options->m_wide;
				std::cout << std::string(options->height(), '\n') << std::flush;
				painter->refresh();
				break;
			}
			case 'j': {
				// make sure the paint loop is locked
				std::cout << repeat(options->height(), cls);
				resized = true;
				options->m_total = !options->m_total;
				std::cout << std::string(options->height(), '\n') << std::flush;
				painter->refresh();
				break;
			}
			case 'l': case 'L': {
				// make sure the paint loop is locked
				std::cout << repeat(options->height(), cls);
				resized = true;
				options->m_lohi = !options->m_lohi;
				std::cout << std::string(options->height(), '\n') << std::flush;
				painter->refresh();
				break;
			}
			case 'r':
				painter->refresh(); // Force refresh
				break;
		}
	}
	return NULL;
}
