#ifndef PAINTLOOP_H
#define PAINTLOOP_H

#define MINCOLUMNWIDTH 16

#include <termios.h>

#include "CmdLineOpts.h"
#include "ColorConfig.h"
#include "MemoryRepresentation.h"

class PaintLoop {
private:
	int m_width, m_terminalColumns, m_maxSamples, m_sampleCounter = 0, m_maxOutputWidth;
	bool m_calculateWidth = true, m_resized = false, m_caughtException = false;

	struct termios m_orig_term_attr;
	struct termios m_new_term_attr;

	ColorConfig *m_colorConfig;

	std::string getOutput(const MemoryRepresentation& sampleMemory, int terminalColumns, int dataColumns) const;

public:
	CmdLineOpts* m_opts;

	PaintLoop(CmdLineOpts* options, ColorConfig* colorConfig);
	void start();
	void resize();
	void refresh();
	void stop();
	void paus();
	void resume();
};

#endif
