#ifndef CMDLINEOPTS_H
#define CMDLINEOPTS_H

#include "ColorConfig.h"

#include <functional>
#include <iterator>
#include <map>
#include <string>
#include <vector>

const int VERBOSITY_BRIEF = 0;
const int VERBOSITY_NORMAL = 1;
const int VERBOSITY_VERBOSE  = 2;
const int SHOW_MEMORY = 1;
const int SHOW_SWAP = 2;
const int SHOW_BOTH = SHOW_MEMORY | SHOW_SWAP;

class CmdLineOpts;
class Argument;

typedef std::function<void(CmdLineOpts* options, const std::string&, const std::string&)> callable;
typedef std::map<const std::string, Argument*> argument_map;

class Argument {
public:
	callable m_function;
	std::string m_shortArg;
	std::vector<std::string> m_longArgs;
	Argument(callable function,
			std::string shortArg,
			std::vector<std::string> longArgs
	):
		m_function(function),
		m_shortArg(shortArg),
		m_longArgs(longArgs) {};
	inline void call(CmdLineOpts* options, const std::string& key, const std::string& value) {
		m_function(options, key, value);
	};
};


class CmdLineOpts {
private:
	void _init();
	int m_tokenizerWordCounter = 0;
	std::string::const_iterator m_tokenizerWordPointer = static_cast<std::string::const_iterator>(0);
	std::string::const_iterator m_tokenizerWordEnd = static_cast<std::string::const_iterator>(0);
	argument_map m_longArgs, m_shortArgs;
	void registerParameter(Argument* argument);

public:
	bool m_lohi;
	bool m_color;
	bool m_frog;
	bool m_total;
	bool m_wide;
	bool m_temporaryWide = false;
	int m_verbosity;
	int m_delay;
	int m_maxSamples;
	int m_maxWidth;
	int m_show;
	long m_unit;

	CmdLineOpts(int argc, const char** argv);
	CmdLineOpts(const ColorConfig& colorConfig, int argc, const char** argv);
	void init(int argc, const char** argv, int start = 0);
	int height() const;
	int columns() const;
	void checkArg(const std::string& str, int i, int argc, const char** argv);
	void fixateHumanReadableUnit(unsigned long total);
	void printAuthors() const;
	void printHelp() const;
	void printLicense() const;
	void printUsage() const;
	void printVersion() const;
	void incrementTokenizer();

	~CmdLineOpts();
};

#endif
