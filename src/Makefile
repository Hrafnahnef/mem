DESTDIR=
PREFIX=/usr
NPROCS = $(shell grep -c 'processor' /proc/cpuinfo)
MAKEFLAGS += -j$(NPROCS)
CC=g++
DEBUGFLAGS=-g -O0
BUILDFLAGS=-std=c++20
LDFLAGS=-pthread -lcurses
SOURCES := mem.cpp MemoryRepresentation.cpp ColorConfig.cpp CmdLineOpts.cpp threadutils.cpp term.cpp PaintLoop.cpp input.cpp
OBJS := $(patsubst %.cpp,%.o,$(SOURCES))
SRCS := $(patsubst %.o,%.cpp,$(OBJS))

EXECUTABLE=mem
MANPAGE=mem.1.gz

all: $(MANPAGE) $(EXECUTABLE)
.PHONY: all

$(MANPAGE):
	@echo 'Compressing man page...'
	@gzip -c mem.manpage > $(MANPAGE)

$(EXECUTABLE): $(OBJS)
	@echo 'Linking...'
	@$(CC) $^ -o $@ $(LDFLAGS)
	@echo 'Done.'

$(OBJS): $(SOURCES)
	@echo "Building $(patsubst %.o,%.cpp,$@)"
	@$(CC) $(BUILDFLAGS) -c $(patsubst %.o,%.cpp,$@) -o $@

clean:
	@echo 'Cleaning...'
	@rm -rf $(EXECUTABLE)
	@rm -rf $(OBJS)
	@rm -rf $(MANPAGE)
	@echo 'Done.'

install:
	@install -m0755 $(EXECUTABLE) $(DESTDIR)$(PREFIX)/bin
	@install -m0644 $(MANPAGE) $(DESTDIR)$(PREFIX)/share/man/man1/
	@mkdir -p -m0755 $(DESTDIR)$(PREFIX)/share/mem/
	@install -m0644 memrc.template $(DESTDIR)$(PREFIX)/share/mem/
