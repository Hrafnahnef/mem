#ifndef MEMORYREPRESENTATION_H
#define MEMORYREPRESENTATION_H

#include "ColorConfig.h"
#include "CmdLineOpts.h"

#include <string>

struct mem {
    unsigned long total;
    unsigned long hightotal;
    unsigned long free;
    unsigned long highfree;
    unsigned long cached;
    unsigned long buffer;
    unsigned long swaptotal;
    unsigned long swapfree;
    unsigned long swapcached;
    unsigned long swapbuffer;
};

class MemoryRepresentation {

private:
	mem m_memory;

	static const unsigned int STR_TOTAL    = 0;
	static const unsigned int STR_USE      = 1;
	static const unsigned int STR_FREE     = 2;
	static const unsigned int STR_BUFFER   = 3;
	static const unsigned int STR_CACHE    = 4;
	static const unsigned int STR_LO_TOTAL = 5;
	static const unsigned int STR_LO_USE   = 6;
	static const unsigned int STR_LO_FREE  = 7;
	static const unsigned int STR_HI_TOTAL = 8;
	static const unsigned int STR_HI_USE   = 9;
	static const unsigned int STR_HI_FREE  = 10;

	const std::string _strings[11][8] = {
		{
			"T", "T:", "Tot: ", "Total: ", "Total memory: ", "Total memory: ", "Total swap: ", "Total: "
		}, {
			"U", "U:", "Use: ", "Used: ", "Used memory: ", "Used memory: ", "Used swap: ", "Total used: "
		}, {
			"F", "F:", "Free ", "Free: ", "Free memory: ", "Free memory: ", "Free swap: ", "Total free: "
		}, {
			"B", "B:", "Buff:", "Buff: ", "Buffer: ", "Buffer: ", "Buffer: ", "Total buffer: "
		}, {
			"C", "C:", "Cach:", "Cache: ", "Cached: ", "Cached: ", "Cached: ", "Total cached: "
		}, {
			"L", "Lo:", "Lo: ", "Lo total: ", "Total low memory: ", "Total low memory: ", "Total low memory: ", "Total low memory: "
		}, {
			"U", "U:", "Use: ", "Used: ", "Used low: ", "Used low memory: ", "Used low memory: ", "Used low memory: "
		}, {
			"F", "F:", "Free ", "Free: ", "Free low: ", "Free low memory: ", "Free low memory: ", "Free low memory: "
		}, {
			"H", "Hi:", "Hi: ", "Hi total: ", "Total high memory: ", "Total high memory: ", "Total high memory: ", "Total high memory: "
		}, {
			"U", "U:", "Use: ", "Used: ", "Used high: ", "Used high memory: ", "Used high memory: ", "Used high memory: "
		}, {
			"F", "F:", "Free ", "Free: ", "Free high: ", "Free high memory: ", "Free high memory: ", "Free high memory: "
		}
	};
	std::string getString(int str, int size, unsigned short int mode) const;
	std::string getLoHiText(int width, bool useColor, const ColorConfig &cc, CmdLineOpts *opts, int line) const;

public:
    static const unsigned short int MODE_MEMORY = 1;
	static const unsigned short int MODE_SWAP   = 2;
	static const unsigned short int MODE_TOTAL  = 3;
	static const unsigned short int MODE_LOHI   = 4;

	MemoryRepresentation();
	bool hasSwap() const;
    std::string getBar(int width, unsigned short int mode, bool useColor, ColorConfig &cc) const;
    std::string getText(int width, unsigned short int mode, bool useColor, ColorConfig &cc, CmdLineOpts *opts, int line) const;
	~MemoryRepresentation();
};

#endif
