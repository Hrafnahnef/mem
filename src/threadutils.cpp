#include "threadutils.h"

void wait_till_next_tick(int decitick) {
  int milliseconds = static_cast<long>(decitick) * 100L;
  struct timespec req = {0};
  req.tv_sec = (milliseconds / 1000L);
  req.tv_nsec = (milliseconds % 1000L) * 1000000L;
  nanosleep(&req, (struct timespec *)NULL);
}
